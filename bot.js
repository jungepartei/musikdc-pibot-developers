"use strict";

const errorlog = require("./data/errors.json")
const thing = require('mathjs')
const maths = thing.parser()
const Discord = require("discord.js")
const started = Date()
const config = require('./config.json')
const bot = new Discord.Client()
const notes = require('./data/notes.json')
const os = require('os')
const prefix = config.prefix
const rb = "```"
const sbl = require("./data/blservers.json")
const ubl = require("./data/blusers.json")
const fs = require("fs")
const warns = require("./data/warns.json")
const queues = {}
const ytdl = require('ytdl-core')
const search = require('youtube-search')
const opts = {
  part: 'snippet',
  maxResults: 10,
  key: config.youtube_api_key
}

function getQueue(guild) {
  if (!guild) return
  if (typeof guild == 'object') guild = guild.id
  if (queues[guild]) return queues[guild]
  else queues[guild] = []
  return queues[guild]
}

var express = require("express")
var app = express();

app.get("/queue/:guildid",function(req,res){
  let queue = getQueue(req.params.guildid);
    if(queue.length == 0) return res.send("Uh oh... No music!");
    let text = '';
    for(let i = 0; i < queue.length; i++){
      text += `${(i + 1)}. ${queue[i].title} | by ${queue[i].requested}\n`
    };
  res.send(text)
})
        app.listen(config.server_port)


function play(msg, queue, song) {
  if (!msg || !queue) return
  if (song) {
    search(song, opts, function(err, results) {
      if (err) return bot.sendMessage(msg, "Video not found please try to use a youtube video.");
      song = (song.includes("https://" || "http://")) ? song : results[0].link
      let stream = ytdl(song, {
        audioonly: true
      })
      let test
      if (queue.length === 0) test = true
      queue.push({
        "title": results[0].title,
        "requested": msg.author.username,
        "toplay": stream
      })
      bot.sendMessage(msg, "Queued **" + queue[queue.length - 1].title + "**")
      if (test) {
        setTimeout(function() {
          play(msg, queue)
        }, 1000)
      }
    })
  } else if (queue.length != 0) {
    bot.sendMessage(msg, `Now Playing **${queue[0].title}** | by ***${queue[0].requested}***`)
    let connection = bot.voiceConnections.get('server', msg.server)
    if (!connection) return
    connection.playRawStream(queue[0].toplay).then(intent => {
      intent.on('error', () => {
        queue.shift()
        play(msg, queue)
      })

      intent.on('end', () => {
        queue.shift()
        play(msg, queue)
      })
    })
  } else {
    bot.sendMessage(msg, 'No more music in queue')
  }
}

function secondsToString(seconds) {
    try {
        var numyears = Math.floor(seconds / 31536000);
        var numdays = Math.floor((seconds % 31536000) / 86400);
        var numhours = Math.floor(((seconds % 31536000) % 86400) / 3600);
        var numminutes = Math.floor((((seconds % 31536000) % 86400) % 3600) / 60);
        var numseconds = Math.round((((seconds % 31536000) % 86400) % 3600) % 60);

        var str = "";
        if(numyears>0) {
            str += numyears + " year" + (numyears==1 ? "" : "s") + " ";
        }
        if(numdays>0) {
            str += numdays + " day" + (numdays==1 ? "" : "s") + " ";
        }
        if(numhours>0) {
            str += numhours + " hour" + (numhours==1 ? "" : "s") + " ";
        }
        if(numminutes>0) {
            str += numminutes + " minute" + (numminutes==1 ? "" : "s") + " ";
        }
        if(numseconds>0) {
            str += numseconds + " second" + (numseconds==1 ? "" : "s") + " ";
        }
        return str;
    } catch(err) {
        console.log("Could not get time")
        return 'Could not get time';
    }
}

bot.on('ready', function() {
  bot.setStatus('online', config.status)
  var msg = `
-----------------------------
Eingeloggt als ${bot.user.name}
-----------------------------`

console.log(msg)
console.log("Loggt ein")
})

bot.on("message", function(message) {
  try{
  if (message.sender.bot) return
  if (message.channel.server === undefined && message.sender != bot.user) {
    bot.sendMessage(message, "Bot only works in Servers, not Private Messages (This is so blacklist system works properely)")

    return;
  }
  if (sbl.indexOf(message.channel.server.id) != -1 && message.content.startsWith(prefix)) {
    bot.sendMessage(message, "This server is blacklisted")
    return
  }
  if (ubl.indexOf(message.sender.id) != -1 && message.content.startsWith(prefix)) {
    bot.sendMessage(message, message.user + "! You are blacklisted and can not use the bot!")
    return
  }
  if (message.content.startsWith(prefix + "ping")) {
    bot.sendMessage(message, "Pong!", function(error, msg) {
      if (!error) {
        bot.updateMessage(msg, "Pong, **" + (msg.timestamp - message.timestamp) + "**ms")
      }
    })
  }
  if(message.content.startsWith(prefix + 'math')) {
    try{
    var res = maths.eval(message.content.split(" ").splice(1).join(" "))
  }catch(err){
    var res = 'Could not calculate'
  }
    bot.sendMessage("```"+message,res+"```")
  }

  

bot.loginWithToken(config.token)
/
